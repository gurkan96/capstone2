terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "gurkan-tf-s3-bucket"
    key = "myapp/state.tfstate"
    region = "eu-central-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.20.1"
    }
  }
}
